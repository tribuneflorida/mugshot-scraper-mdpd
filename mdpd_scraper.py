#!/usr/bin/python
"""
Miami-Dade County Police Department (MDPD) Mug Shot Scraper
Author: Danny Sanchez, dsanchez@sun-sentinel.com, 954-356-4818
Description: Retrieves and stores arrest records and mug shots from Miami-Dade County.
Data sources are from Miami-Dade's Open Data API and a Tronc FTP server where Miami-Dade Police Dept. sends us a daily zip file.
This script combines the inmate ID and photos from the FTP file with the cleaner API-sourced data (which lacks the inmate id).
Then everything is processed and stored for use in the mug shots viewing app.
"""
# import json
import os
from datetime import date, timedelta, datetime
import zipfile
from ftplib import FTP
import logging
import time
import re
from slacker_log_handler import SlackerLogHandler, NoStacktraceFormatter
import requests
import pendulum
from bs4 import BeautifulSoup
import psycopg2
import pprint


class MDPDScraper:

    def __init__(self, target_date=None):

        self.inmate_data = {}  # Placeholder dictionary for arrest data combined from FTP and API data.

        if target_date is None:
            self.date_to_get = date.today()
            # self.date_to_get = date.today() - timedelta(1)  # Default to yesterday.
            """
            The daily file only has data from the day before.
            """
        else:
            self.date_to_get = target_date

        self.zip_file_name = "mugshots-" + self.date_to_get.strftime('%Y%m%d') + ".zip"

        self.database_creds = {
            'dbname': 'sun_sentinel_mugshots',
            'user': 'ssmugs_scraper',
            'password': 'NeTEmeX#pr5S',
            'host': 'sunsentinelmugsprod.csjwyqw2b93i.us-east-1.rds.amazonaws.com'
        }

        # For logging.
        self.start_time = time.time()
        self.how_many_insert_errors = 0
        logging.basicConfig(filename='mdpd-scraper.log', level=logging.INFO)  # For text log

        # For Slack logging to sunsentinel.slack.com #application-logs channel using slacker_log_handler library.
        self.slack_handler = SlackerLogHandler('xoxp-2869259434-44738060161-413255226883-e2760f72927c9690cec843df154bd98f', 'sfl-mugshot-log', stack_trace=True, username="MDPD Mugshots Scraper", icon_url="http://www.trbimg.com/img-5ab51cc3/turbine/sfl-miami-dade-county-mug-shot-scraper-icon-20180323/600")
        self.slack_logger = logging.getLogger('debug_application')
        self.slack_logger.addHandler(self.slack_handler)
        slack_formatter = NoStacktraceFormatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.slack_handler.setFormatter(slack_formatter)

    def get_arrest_ftp_file(self, date_to_get=None):
        """
        Connect to the Tronc FTP server and retrieve the zip file of arrests and mugshots for a particular date.
        """
        print("Retrieving arrest file from FTP server...")
        ftp = FTP('ftp.tribpub.com')  # connect to host, default port
        print(ftp.login('ss_mugshots_miamidade', 'Miamidade$$'))
        file = open(self.zip_file_name, 'wb')
        ftp.retrbinary('RETR %s' % self.zip_file_name, file.write)
        ftp.quit()

    def extract_zip_file_contents(self):
        """
        Extracts the arrest text files and mugshot images from the zip file.
        Places the images in the proper directory and renames the image files with the convention needed for the mugshot application.
        """
        print("Extracting files from downloaded archive...")
        zfile = zipfile.ZipFile(self.zip_file_name)
        zfile.extract("ABCBOOK.TXT")  # List of inmates
        zfile.extract("ABCCHRG.TXT")  # List of charges to inmate IDs

        # Get the JPG files, rename them for the Flask app and put them in the correct directory.
        # i.e. 012345.JPG as mdpd-012345.jpg
        image_directory = "/var/www/html/mugshots/static/mugshot-images/"
        for file in zfile.namelist():
            if "JPG" in file:
                # for image_file in image_filenames:
                zfile.extract(file, image_directory)
                os.rename(image_directory + file, image_directory + "mdpd-" + file.replace("JPG", "jpg"))

    def parse_fixed_width_inmate_text_file(self):
        """
        Parses out the inmate data from the ugly fixed-width file provided by Dade County.
        The Dade open data API lacks the inmate id and may have incomplete charges.
        """
        print("Parsing arrest files...")

        # TODO - Call the file directly out of the zipfile.
        with open("ABCBOOK.TXT") as text_file:
            lines = text_file.readlines()
            for line in lines:
                if line[122] == "B":
                    inmate_race = "Black"
                elif line[122] == "W":
                    inmate_race = "White"
                else:
                    inmate_race = line[122]

                # Parse the sex
                if line[123] == "M":
                    inmate_sex = "Male"
                elif line[123] == "F":
                    inmate_sex = "Female"
                else:
                    inmate_sex = line[123]

                # TODO - Convert the booking date into DB-friendly format

                # Put everything into the master inmate dictionary
                inmate_id = line[0:9].strip()

                # Check of DOB.
                try:
                    dob = datetime.strptime(line[114:122], '%Y%m%d').date()
                except:
                    dob = ""

                #Check of admission date
                try:
                    admitted_datetime = pendulum.parse(line[106:114], tz="US/Eastern").to_datetime_string()
                except:
                    admitted_datetime = ""

                inmate = {
                    inmate_id: {
                        "slug": "mdpd-" + inmate_id,
                        "first_name": line[9:20].strip().title(),
                        "last_name": line[21:50].strip().title(),
                        "address": line[51:74].strip(),
                        "address_unit": line[75:79].strip(),
                        "address_city": line[80:94].strip().title(),
                        "address_state": line[95:97],
                        "address_zip": line[97:105][0:5].strip(),
                        "admitted_datetime": admitted_datetime,
                        "dob": dob,
                        "race": inmate_race,
                        "sex": inmate_sex,
                        "arresting_agency": line[124:200].strip().title(),
                        "mugshot_filename": "mdpd-" + inmate_id + ".jpg",
                        "charges": []
                    }
                }
                self.inmate_data.update(inmate)
                inmate = inmate_sex = inmate_race = inmate_id = None

    def parse_fixed_width_charges_text_file(self):

        with open("ABCCHRG.TXT") as text_file:
            lines = text_file.readlines()
            for line in lines:
                inmate_id = line[0:9]
                # case_number = line[9:20]
                # number_of_charge = line[21:25]
                # statute_number = line[26:31]
                charge = line[42:200].strip()
                if self.inmate_data[inmate_id]:
                    self.inmate_data[inmate_id]["charges"].append(charge)
                    # print("Inmate " + inmate_id + " exists" + ": " + charge)
                else:
                    print("Inmate " + inmate_id + " not in inmate file but is in charges file.")

    def get_arrest_api_data(self):
        """
        Get all the arrests for a given date from Miami-Dade's Open Data portal.
        """
        arrest_api_url = "https://opendata.miamidade.gov/resource/k7xd-qgzt.json?bookdate=" + self.date_to_get.strftime('%Y-%m-%d') + "T00:00:00.000"
        print("Querying " + arrest_api_url)
        records = json.loads(requests.get(arrest_api_url).text)
        print(records)

        for record in records:
            self.arrest_api_data.append({
                "last_name": record["defendant"].split()[0].replace(",", "").title(),
                "first_name": record["defendant"].split()[1].replace(",", "").title(),
                "dob": record["dob"].replace("T00:00:00.000", ""),
                "bookdate": record["bookdate"].replace("T00:00:00.000", ""),
                "address": record["location_1_address"]
            })

        print(self.arrest_api_data)
        # print(type(records))

    def search_mdpd_site_inmate(self):
        # TODO - Replace this with a loop of the names.
        # Use Marcus Williams as example.
        counter = 0
        for inmate in self.arrest_api_data:
            counter += 1
            if counter < 20:
                time.sleep(1)
                search_payload = {
                    "L_NAME": inmate["last_name"],
                    "F_NAME": inmate["first_name"],
                    "PROCESS": None,
                    "F311": None,
                    "INITIME": None
                }
                print(search_payload)

                response = requests.post("http://egvsys.miamidade.gov:1608/wwwserv/crts/IPSAWNSL.DIA", data=search_payload)
                soup = BeautifulSoup(response.text, "html.parser")
                try:
                    inmate_url = soup.find("a", href=re.compile(r"^CJSAWNSX"))["href"]
                    inmate["inmate_url"] = "http://egvsys.miamidade.gov:1608/wwwserv/crts/" + inmate_url.replace(" ", "%20").replace("\r\n", "")
                except:
                    inmate["inmate_url"] = None

        pprint.pprint(self.arrest_api_data)

    def store_inmate_data(self):
        """
        If an inmate is already in the database (an UPSERT), only update the record if there is no mugshot image, just in case a page disappears from the jail website on a subsequent scrape.
        """
        # Write to a local JSON file.
        # with open("arrest-data/" + datetime.strptime(self.latest_search_date.replace("%2F", "/"), "%m/%d/%Y").strftime("%Y%m%d") + "-arrests.json", "w+") as fp:
        #    json.dump(self.inmate_data, fp)

        # Insert arrest records into the database.
        conn = None
        try:
            conn = psycopg2.connect(dbname=self.database_creds["dbname"], user=self.database_creds["user"], password=self.database_creds["password"], host=self.database_creds["host"])
            conn.autocommit = True

        except:
            print("Curses! Error connecting to the arrests database!")
            now = pendulum.now('US/Eastern').to_iso8601_string()
            db_connect_error_msg = now + ", Error connecting to the arrests database."
            logging.info(db_connect_error_msg)
            self.slack_logger.error(db_connect_error_msg)

        if conn is not None:
            print("Writing arrest data to database...")
            cur = conn.cursor()

            for inmate_id, inmate_details in self.inmate_data.items():
                try:
                    # UPSERT SQL. Don't update the record if a mugshot image was previously found. It's possible a detail record page might have been removed from the site after a previous scrape.
                    response = cur.execute("INSERT INTO arrests (id, inmate_id, last_name, first_name, race, sex, data_source, arresting_agency, admitted_datetime, charges, dob, county, mugshot_filename) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (id) DO UPDATE SET (id, inmate_id, last_name, first_name, race, sex, data_source, arresting_agency, admitted_datetime, charges, dob, county, mugshot_filename) = (EXCLUDED.id, EXCLUDED.inmate_id, EXCLUDED.last_name, EXCLUDED.first_name, EXCLUDED.race, EXCLUDED.sex, EXCLUDED.data_source, EXCLUDED.arresting_agency, EXCLUDED.admitted_datetime, EXCLUDED.charges, EXCLUDED.dob, EXCLUDED.county, EXCLUDED.mugshot_filename)", (inmate_details["slug"], inmate_id, inmate_details["last_name"], inmate_details["first_name"], inmate_details["race"], inmate_details["sex"], "MDPD", inmate_details["arresting_agency"], inmate_details["admitted_datetime"], inmate_details["charges"], inmate_details["dob"], "Miami-Dade", inmate_details["mugshot_filename"]))
                    # print(response)

                except psycopg2.Error as e:
                    database_error_message = "Error inserting " + inmate_details["last_name"] + ", " + inmate_details["first_name"] + " (" + inmate_id + ")"
                    self.how_many_insert_errors += 1  # Logging
                    print(database_error_message)
                    print (e.pgerror)
                    print (e.pgcode)
                    pass

    def delete_downloaded_files(self):
        """
        Delete the large zip file downloaded from the FTP server.
        """
        # Delete the other text file
        print("Cleaning up downloaded files...")
        try:
            os.remove(self.zip_file_name)
            os.remove("ABCBOOK.TXT")
            os.remove("ABCCHRG.TXT")
        except:
            print("Error removing files.")
        # TODO Loop through the image directory and delete all images.

    def log_results(self):
        """
        Log data about how the scrape went.
        """
        now = pendulum.now('US/Eastern').to_iso8601_string()
        elapsed_time = time.time() - self.start_time
        log_message = "Arrests: " + str(len(self.inmate_data)) + ", DB Errors: " + str(self.how_many_insert_errors) + " Date Searched: " + self.date_to_get.strftime('%m-%d-%Y') + ", Scraper Elapsed Time: " + str(round(elapsed_time, 2)) + ", " + now
        print(log_message)
        logging.info(log_message)
        self.slack_logger.info(log_message)


mdpd = MDPDScraper()
mdpd.get_arrest_ftp_file()
mdpd.extract_zip_file_contents()
mdpd.parse_fixed_width_inmate_text_file()
mdpd.parse_fixed_width_charges_text_file()
mdpd.store_inmate_data()
mdpd.log_results()
mdpd.delete_downloaded_files()
# pprint.pprint(mdpd.inmate_data)
